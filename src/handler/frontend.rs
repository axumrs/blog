use askama::Template;
use axum::response::Html;

use crate::{error::AppError, html::frontend::IndexTemplate, Result};

pub async fn index() -> Result<Html<String>> {
    let tpl = IndexTemplate {};
    let out = tpl.render().map_err(AppError::from)?;
    Ok(Html(out))
}
