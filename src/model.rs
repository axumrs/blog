use serde::Serialize;
use tokio_pg_mapper_derive::PostgresMapper;

pub struct AppState {
    pub pool: deadpool_postgres::Pool,
    pub rdc: redis::Client,
}

#[derive(Serialize, PostgresMapper)]
#[pg_mapper(table = "topics")]
pub struct Topic {
    pub id: i64,
    pub category_id: i32,
    pub hit: i32,
    pub title: String,
    pub summary: String,
    pub dateline: u32,
    pub is_del: bool,
}

#[derive(Serialize, PostgresMapper)]
#[pg_mapper(table = "topic_contents")]
pub struct TopicContent {
    pub topic_id: i64,
    pub markdown: String,
    pub html: String,
}

#[derive(Serialize, PostgresMapper)]
#[pg_mapper(table = "topics")]
pub struct TopicID {
    pub id: i64,
}

#[derive(Serialize, PostgresMapper)]
#[pg_mapper(table = "categoies")]
pub struct Category {
    pub id: i32,
    pub name: String,
    pub is_del: bool,
}

#[derive(Serialize, PostgresMapper)]
#[pg_mapper(table = "categoies")]
pub struct CategoryID {
    pub id: i32,
}

#[derive(Serialize, PostgresMapper)]
#[pg_mapper(table = "tags")]
pub struct Tag {
    pub id: i32,
    pub name: String,
    pub is_del: bool,
}
#[derive(Serialize, PostgresMapper)]
#[pg_mapper(table = "tags")]
pub struct TagID {
    pub id: i32,
}

#[derive(Serialize, PostgresMapper)]
#[pg_mapper(table = "users")]
pub struct User {
    pub id: i32,
    pub username: String,
    pub password: String,
    pub is_admin: bool,
    pub is_del: bool,
}

#[derive(Serialize, PostgresMapper)]
#[pg_mapper(table = "users")]
pub struct UserID {
    pub id: i32,
}
